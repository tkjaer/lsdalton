#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > cartes_psb_min.info <<'%EOF%'
   dft_blyp_molhes 
   -------------
   Molecule:         H2O2
   Wave Function:    DFT / BLYP / 6-31G** / Ahlrichs-Coulomb-Fit
   Test Purpose:     Test geometry optimizer in Cartesian coordinates with 
   a diagonal initial Hessian updated by PSB formula, McWeenie's purification
   used for constructing initial guess
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > cartes_psb_min.mol <<'%EOF%'
ATOMBASIS
H2O2
First order minimization in Cartesian coordinates
Atomtypes=2 Generators=0
Charge=8.0 Atoms=2 Bas=6-31G** Aux=Ahlrichs-Coulomb-Fit
O    -0.2000000000        1.3500000000        0.1000000000
O    -0.2000000000       -1.3500000000       -0.1000000000
Charge=1.0 Atoms=2 Bas=6-31G** Aux=Ahlrichs-Coulomb-Fit
H     0.6500000000        1.6000000000        1.8000000000
H     0.6500000000       -1.6000000000       -1.8000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > cartes_psb_min.dal <<'%EOF%'
**INTEGRALS
.THRESH
1.D-10
**OPTIMI
.CARTES
.PSB
.INITEV
0.8D0
.BAKER
**WAVE FUNCTIONS
.DFT
BLYP
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.RESTART
.CONVTHR
3.0d-5
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > cartes_psb_min.check
cat >> cartes_psb_min.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Energy at final geometry is * \: * \-151\.51849[8-9]" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Number of energies
CRIT3=`$GREP "Final * DFT energy" $log | wc -l` 
TEST[3]=`expr   $CRIT3`
CTRL[3]=15
ERROR[3]="Wrong number of geometry steps"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=0
ERROR[4]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
