#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_DaLink2.info <<'%EOF%'
   LSDALTON_DaLink2
   -------------
   Molecule:         C16H2
   Wave Function:    HF/STO-2G
   Test Purpose:     Check DaLink integrals
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_DaLink2.mol <<'%EOF%'
BASIS
STO-2G
C16H2 molecule based on an optimized C40H2 
structur using b3lyp/6-311+g(d,p)
Atomtypes=2 Nosymmetry Angstrom
Charge=6. Atoms=16
C         0.00000000      0.00000000      1.08290700
C         0.00000000      0.00000000      2.29461600
C         0.00000000      0.00000000      3.64429100
C         0.00000000      0.00000000      4.86890900
C         0.00000000      0.00000000      6.20522000
C         0.00000000      0.00000000      7.43493100
C         0.00000000      0.00000000      8.76635300
C         0.00000000      0.00000000      9.99834400
C         0.00000000      0.00000000     11.32744700
C         0.00000000      0.00000000     12.56059500
C         0.00000000      0.00000000     13.88848500
C         0.00000000      0.00000000     15.12226000
C         0.00000000      0.00000000     16.44947800
C         0.00000000      0.00000000     17.68360100
C         0.00000000      0.00000000     19.01043900
C         0.00000000      0.00000000     20.24475800
Charge=1. Atoms=2
H         0.00000000      0.00000000      0.00000000
H         0.00000000      0.00000000     21.32766500

%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_DaLink2.dal <<'%EOF%'
**INTEGRALS
.THRESH
1.0D-9
.DALINK
**WAVE FUNCTIONS
.HF
*DENSOPT
.CONVTHR
8.0D-6
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_DaLink2.check
cat >> LSDALTON_DaLink2.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-581\.157191268" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
