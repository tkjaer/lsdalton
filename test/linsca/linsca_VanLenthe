#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_VanLenthe.info <<'%EOF%'
   linsca_VanLenthe
   -------------
   Molecule:         H2O
   Wave Function:    HF
   Test Purpose:     Check Linsca 
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_VanLenthe.mol <<'%EOF%'
BASIS
3-21G
water R(OH) = 0.95Aa , <HOH = 109 deg.
Distance in Aangstroms
   2     0         *
        8.    1
O      0.00000   0.00000   0.00000
        1.    2
H      0.55168   0.77340   0.00000
H      0.55168  -0.77340   0.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_VanLenthe.dal <<'%EOF%'
**GENERAL
.TIME
**WAVE FUNCTIONS
.HF
*DENSOPT
.VanLenthe
.NVEC
8
.CONVTHR
3.D-5
.START
H1DIAG
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_VanLenthe.check
cat >> linsca_VanLenthe.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY test
CRIT1=`$GREP "Final HF energy: * -75.585422" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
