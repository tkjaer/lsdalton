#!/usr/bin/env python

import os
import sys
import shutil

sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
from runtest_lsdalton import Filter, TestRun

test = TestRun(__file__, sys.argv)

# Input files:
DAL1 = "decrimp2_crashcalc"
DAL2 = "decrimp2_restart"
MOL  = "B3H3"

# Define filter for restarted calculation
# =======================================
f = Filter()
f.add(string = 'Correlation energy',
      rel_tolerance = 1.0e-3)

f.add(string = 'Total RIMP2 energy',
      rel_tolerance = 1.0e-3)

f.add(string = ' AFOC',
      rel_tolerance = 1.0e-3)

f.add(from_string = 'RI-MP2 occupied single energies',
      to_string   = 'RI-MP2 Lagrangian correlation energy',
      abs_tolerance = 1.0e-3)

f.add(string = 'Allocated memory (TOTAL)')

f.add(string = 'Memory in use for array4',
      rel_tolerance = 1.0e-9)

# Build list of files to save for restart
# =======================================
HF_restart  = ['cmo_orbitals.u','lcm_orbitals.u','dens.restart','fock.restart','overlap.restart']
DEC_restart = ['atomicfragmentsdone.info','atomicfragments.info','estimated_fragenergies.info']

get_restart = ['-get','"'] + HF_restart + DEC_restart + ['"']
put_restart = ['-put','"'] + HF_restart + DEC_restart + ['"']

arg_restart = " ".join(get_restart)


# Run first calculation with CRASHCALC keyword
# ============================================
test.run([DAL1+'.dal'], [MOL+'.mol'], {'out': f}, args = arg_restart,
        accepted_errors = ['Crashed Calculation due to .CRASHCALC keyword'])

name = "_".join([DAL1,MOL]) 

# Remove prefix from restart files
# ================================
for filename in HF_restart:
    tmp = ".".join([name,filename])
    shutil.move(tmp,filename)

for filename in DEC_restart:
    tmp = ".".join([name,filename])
    shutil.move(tmp,filename)


arg_restart = " ".join(put_restart)

# Run second calculation with RESTART keyword
# ===========================================
test.run([DAL2+'.dal'], [MOL+'.mol'], {'out': f}, args = arg_restart)


# Delete files
# ============
for filename in HF_restart:
    os.remove(filename)

for filename in DEC_restart:
    os.remove(filename)

sys.exit(test.return_code)

