     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | simensr
     Host                     | 1x-193-157-210-125.uio.no
     System                   | Darwin-14.5.0
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/local/bin/gfortran
     Fortran compiler version | GNU Fortran (GCC) 5.0.0 20141012 (experimental)
     C compiler               | /usr/local/bin/gcc
     C compiler version       | gcc (GCC) 5.0.0 20141012 (experimental)
     C++ compiler             | /usr/local/bin/g++
     C++ compiler version     | g++ (GCC) 5.0.0 20141012 (experimental)
     BLAS                     | /usr/lib/libblas.dylib
     LAPACK                   | /usr/lib/liblapack.dylib
     Static linking           | OFF
     Last Git revision        | 0fdb8e13de5b78695e849f3fbae15032ecd5b3af
     Git branch               | Simen/Chemshell_lslib
     Configuration time       | 2017-03-21 13:19:28.214100
  
 Ground state molecular gradient calculations are carried out.
    The Functional chosen is a GGA type functional
    The Functional chosen contains an exact exchange contribution
    with the weight:  2.00000000E-01

         Start simulation
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    ATOMBASIS                               
    Mimicing the ChemShell LSDALTON interfac
    Water molecule in the field of another w
    Atomtypes=6 Charge=0 Spherical                                                                                          
    Charge=8 Atoms=1 Basis=3-21G Aux=df-def2                                                                                
    o1 0.0000000000 0.0000000000 0.0000000000                                                                               
    Charge=1 Atoms=1 Basis=3-21G Aux=df-def2                                                                                
    h2 0.0000000000 -1.4207748912 1.0737442022                                                                              
    Charge=1 Atoms=1 Basis=3-21G Aux=df-def2                                                                                
    h3 -0.0000000000 1.4207748912 1.0737442022                                                                              
    Charge=-1.7960000000 Atoms=1 pointcharge Aux=df-def2                                                                    
    bq1 -4.7459987607 0.0000000000 -2.7401036621                                                                            
    Charge=0.8980000000 Atoms=1 pointcharge Aux=df-def2                                                                     
    bq2 -3.1217528345 0.0000000000 -2.0097934033                                                                            
    Charge=0.8980000000 Atoms=1 pointcharge Aux=df-def2                                                                     
    bq3 -4.4867611522 0.0000000000 -4.5020127872                                                                            
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    ATOMBASIS
    Mimicing the ChemShell LSDALTON interface, see chemshell code (chemsh/src/interface_lsdalton/lsdalto
    Water molecule in the field of another water molecule, combining molecule and dalton input in one fi
    Atomtypes=6 Charge=0 Spherical
    Charge=8 Atoms=1 Basis=3-21G Aux=df-def2
    o1 0.0000000000 0.0000000000 0.0000000000
    Charge=1 Atoms=1 Basis=3-21G Aux=df-def2
    h2 0.0000000000 -1.4207748912 1.0737442022
    Charge=1 Atoms=1 Basis=3-21G Aux=df-def2
    h3 -0.0000000000 1.4207748912 1.0737442022
    Charge=-0.7960000000 Atoms=1 pointcharge Aux=df-def2
    bq1 -4.7459987607 0.0000000000 -2.7401036621
    Charge=0.3980000000 Atoms=1 pointcharge Aux=df-def2
    bq2 -3.1217528345 0.0000000000 -2.0097934033
    Charge=0.3980000000 Atoms=1 pointcharge Aux=df-def2
    bq3 -4.4867611522 0.0000000000 -4.5020127872
    
    **GENERAL
    **INTEGRAL
    .DENSFIT
    .NOBQBQ
    **WAVE FUNCTIONS
    .DFT
    B3LYP
    *DENSOPT
    .CONVDYN
    TIGHT
    .MAXIT
    30
    **RESPONS
    *MOLGRA
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :      6
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 o1     8.000 3-21G                     15        9 [6s3p|3s2p]                                  
          2 h2     1.000 3-21G                      3        2 [3s|2s]                                      
          3 h3     1.000 3-21G                      3        2 [3s|2s]                                      
          4 bq1   -1.796 pointcharge                0        0                                              
          5 bq2    0.898 pointcharge                0        0                                              
          6 bq3    0.898 pointcharge                0        0                                              
    ---------------------------------------------------------------------
    total         10                               21       13
    ---------------------------------------------------------------------
                      
                      
    Atoms and basis sets
      Total number of atoms        :      6
      THE  AUXILIARY is on R =   2
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 o1     8.000 df-def2                   97       77 [14s10p6d2f1g|10s8p4d2f1g]                   
          2 h2     1.000 df-def2                   20       18 [4s2p2d|2s2p2d]                              
          3 h3     1.000 df-def2                   20       18 [4s2p2d|2s2p2d]                              
          4 bq1   -1.796 pointcharge                0        0                                              
          5 bq2    0.898 pointcharge                0        0                                              
          6 bq3    0.898 pointcharge                0        0                                              
    ---------------------------------------------------------------------
    total         10                              137      113
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       13
      Auxiliary basisfunctions           :      113
      Primitive Regular basisfunctions   :       21
      Primitive Auxiliary basisfunctions :      137
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)
    The Functional chosen is a GGA type functional
    The Functional chosen contains a exact exchange contribution
    with the weight:  2.00000000E-01
     
    The Exchange-Correlation Grid specifications:
    Radial Quadrature : Treutler-Ahlrichs M4-T2 scheme
                        (J. Chem. Phys. (1995) 102, 346).
                        Implies also that the angular integration quality becomes Z-dependant
    Space partitioning: Stratmann-Scuseria-Frisch partitioning scheme
                        Chem. Phys. Lett. (1996) vol. 213 page 257,
                        Combined with a blockwise handling of grid points
                        J. Chem. Phys. (2004) vol 121, page 2915.
                        Useful for large molecules.

    We use grid pruning according to Mol. Phys. (1993) vol 78 page 997

     DFT LSint Radial integration threshold:   0.5012D-13
     DFT LSint integration order range     :   [  5:  35]
     Hardness of the partioning function   :   3
     DFT LSint screening thresholds        :   0.10D-08   0.20D-09   0.20D-11
     Threshold for number of electrons     :   0.10D-02
     The Exact Exchange Factor             : 0.2000D+00

    You have requested Augmented Roothaan-Hall optimization
    => explicit averaging is turned off!

    Expand trust radius if ratio is larger than:            0.75
    Contract trust radius if ratio is smaller than:         0.25
    On expansion, trust radius is expanded by a factor      1.20
    On contraction, trust radius is contracted by a factor  0.70

    Maximum size of subspace in ARH linear equations:       2

    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          

    Maximum size of Fock/density queue in averaging:    7
 
    Due to the tightend SCF convergence threshold we also tighten the integral Threshold
    with a factor:      0.100000

    Dynamic convergence threshold for gradient:   0.32E-05
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Since the input basis set is a segmented contracted basis we
    perform the integral evaluation in the more efficient
    standard input basis and then transform to the Grand 
    Canonical basis, which is general contracted.
    You can force the integral evaluation in Grand 
    Canonical basis by using the keyword
    .NOGCINTEGRALTRANSFORM
     
    The Overall Screening threshold is set to              :  1.0000E-09
    The Screening threshold used for Coulomb               :  1.0000E-11
    The Screening threshold used for Exchange              :  1.0000E-09
    The Screening threshold used for One-electron operators:  1.0000E-16
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis

    End of configuration!

 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 

    Level 1 atomic calculation on 3-21G Charge   8
    ================================================
  
    Total Number of grid points:       14368

    Max allocated memory, Grid                    1.038 MB

    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1    -70.0119502629    0.00000000000    0.00      0.00000    0.00    7.590E+00 ###
      2    -73.1931430793   -3.18119281635    0.00      0.00000    0.00    5.067E+00 ###
      3    -74.4673129882   -1.27416990890   -1.00      0.00000    0.00    4.111E-01 ###
      4    -74.4744784151   -0.00716542696   -1.00      0.00000    0.00    9.722E-03 ###
      5    -74.4744826311   -0.00000421596   -1.00      0.00000    0.00    1.090E-03 ###
      6    -74.4744826806   -0.00000004955   -1.00      0.00000    0.00    9.145E-05 ###

    Level 1 atomic calculation on 3-21G Charge   1
    ================================================
  
    Total Number of grid points:       10026

    Max allocated memory, Grid                  753.473 kB

    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1     -0.4346854736    0.00000000000    0.00      0.00000    0.00    8.296E-02 ###
      2     -0.4359224029   -0.00123692931    0.00      0.00000    0.00    5.520E-03 ###
      3     -0.4359279332   -0.00000553036   -1.00      0.00000    0.00    2.332E-05 ###
 
    Matrix type: mtype_dense

    First density: Atoms in molecule guess

    Total Number of grid points:       34420

    Max allocated memory, Grid                    1.934 MB

    Iteration 0 energy:      -76.010522522402
 
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  3.16227766E-06
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1    -75.8857489048    0.00000000000    0.00      0.00000    0.00    4.522E-01 ###
      2    -75.8857489048    0.00000000000    0.00      0.00000   -0.00    5.679E-01 ###
      3    -75.9163647023   -0.03061579750    0.00      0.00000    0.50    3.828E-01 ###
      4    -75.9271828734   -0.01081817118    0.00      0.00000   -0.00    3.014E-01 ###
      5    -75.9557480514   -0.02856517800    0.00      0.00000   -0.00    5.272E-03 ###
      6    -75.9557814328   -0.00003338134    0.00      0.00000   -0.00    1.204E-03 ###
      7    -75.9557820149   -0.00000058210    0.00      0.00000   -0.00    3.929E-05 ###
      8    -75.9557820154   -0.00000000054    0.00      0.00000   -0.00    1.334E-06 ###
    SCF converged in      8 iterations
    >>>  CPU Time used in SCF iterations is   4.09 seconds
    >>> wall Time used in SCF iterations is   4.10 seconds

    Total no. of matmuls in SCF optimization:        758

    Number of occupied orbitals:       5
    Number of virtual orbitals:        8

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     4 iterations!

    Calculation of virtual orbital energies converged in     4 iterations!

     E(LUMO):                         0.082583 au
    -E(HOMO):                        -0.303233 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.385816 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2    0.00000000000    0.0000   -0.0000    0.0000000
      3   -0.03061579750    0.0000    0.5000    0.0000000
      4   -0.01081817118    0.0000   -0.0000    0.0000000
      5   -0.02856517800    0.0000   -0.0000    0.0000000
      6   -0.00003338134    0.0000   -0.0000    0.0000000
      7   -0.00000058210    0.0000   -0.0000    0.0000000
      8   -0.00000000054    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1           -75.88574890475950951441      0.452235633600629D+00
        2           -75.88574890475950951441      0.567901312361969D+00
        3           -75.91636470226133326378      0.382790714122458D+00
        4           -75.92718287343856786720      0.301430654685447D+00
        5           -75.95574805143894536741      0.527155699246025D-02
        6           -75.95578143277899130226      0.120445030376324D-02
        7           -75.95578201487931835345      0.392856644924113D-04
        8           -75.95578201542211616015      0.133411250235004D-05

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final DFT energy:                      -75.955782015422
          Nuclear repulsion:                       9.840685946273
          Electronic energy:                     -85.796467961695




        *************************************************************
        *            MOLECULAR GRADIENT RESULTS (in a.u.)           *
        *************************************************************




                   nuclear rep contribution to gradient
                   ------------------------------------

   o1      -0.1494116412            0.0000000000            2.8728478755
   h2      -0.0040666132            2.1420868405           -1.5339404385
   h3      -0.0040666132           -2.1420868405           -1.5339404385
   bq1     -0.4840745251            0.0000000000           -0.2952625911
   bq2      0.4953470024            0.0000000000            0.3385615932
   bq3      0.1462723903            0.0000000000            0.1517339994


                     Coulomb contribution to gradient
                     --------------------------------

   o1       0.0017646185            0.0000000000            3.5772109921
   h2      -0.0008823093            2.1953363990           -1.7886054961
   h3      -0.0008823093           -2.1953363990           -1.7886054961
   bq1      0.0000000000            0.0000000000            0.0000000000
   bq2      0.0000000000            0.0000000000            0.0000000000
   bq3      0.0000000000            0.0000000000            0.0000000000


                    exchange contribution to gradient
                    ---------------------------------

   o1      -0.0000314007           -0.0000000000           -0.0978752000
   h2       0.0000157003           -0.0602884470            0.0489376000
   h3       0.0000157003            0.0602884470            0.0489376000
   bq1      0.0000000000            0.0000000000            0.0000000000
   bq2      0.0000000000            0.0000000000            0.0000000000
   bq3      0.0000000000            0.0000000000            0.0000000000


                   twoElectron contribution to gradient
                   ------------------------------------

   o1       0.0017332178            0.0000000000            3.4793357921
   h2      -0.0008666089            2.1350479520           -1.7396678961
   h3      -0.0008666089           -2.1350479520           -1.7396678961
   bq1      0.0000000000            0.0000000000            0.0000000000
   bq2      0.0000000000            0.0000000000            0.0000000000
   bq3      0.0000000000            0.0000000000            0.0000000000


                   nuc-el attr contribution to gradient
                   ------------------------------------

   o1       0.1669788214           -0.0000000000           -7.0635239866
   h2       0.0027882247           -4.7173961117            3.6337458652
   h3       0.0027882247            4.7173961117            3.6337458652
   bq1      0.5003633939           -0.0000000000            0.2977892367
   bq2     -0.5217231762            0.0000000000           -0.3471638107
   bq3     -0.1511954884           -0.0000000000           -0.1545931697


                  exchange-corr contribution to gradient
                  --------------------------------------

   o1       0.0001239165            0.0000000000           -0.2792278394
   h2      -0.0000620166           -0.1726847128            0.1396061572
   h3      -0.0000620166            0.1726847128            0.1396061572
   bq1      0.0000000000            0.0000000000            0.0000000000
   bq2      0.0000000000            0.0000000000            0.0000000000
   bq3      0.0000000000            0.0000000000            0.0000000000


                     kinetic contribution to gradient
                     --------------------------------

   o1      -0.0013900966            0.0000000000            0.6922234494
   h2       0.0006950483            0.4617167944           -0.3461117247
   h3       0.0006950483           -0.4617167944           -0.3461117247
   bq1      0.0000000000            0.0000000000            0.0000000000
   bq2      0.0000000000            0.0000000000            0.0000000000
   bq3      0.0000000000            0.0000000000            0.0000000000


                  reorthonomal contribution to gradient
                  -------------------------------------

   o1      -0.0003694848           -0.0000000000            0.3632181404
   h2       0.0001847424            0.1928309598           -0.1816090702
   h3       0.0001847424           -0.1928309598           -0.1816090702
   bq1      0.0000000000            0.0000000000            0.0000000000
   bq2      0.0000000000            0.0000000000            0.0000000000
   bq3      0.0000000000            0.0000000000            0.0000000000


                         Molecular gradient (au)
                         -----------------------

   o1       0.0176647331            0.0000000000            0.0648734315
   h2      -0.0013272234            0.0416017222           -0.0279771071
   h3      -0.0013272234           -0.0416017222           -0.0279771071
   bq1      0.0162888688           -0.0000000000            0.0025266456
   bq2     -0.0263761738            0.0000000000           -0.0086022176
   bq3     -0.0049230980           -0.0000000000           -0.0028591703


 RMS gradient norm (au):      0.0242953987574005


    >>>  CPU Time used in LSDALTON RSP is   0.69 seconds
    >>> wall Time used in LSDALTON RSP is   0.69 seconds
 *****************************************************
 **     CPU-TIME USED IN LSDALTON RESPONSE:   0.69204800000000066         **
 *****************************************************
    Total no. of matmuls used:                       792
    Total no. of Fock/KS matrix evaluations:           9
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                          1.972 MB
      Max allocated memory, type(matrix)                 242.168 kB
      Max allocated memory, real(realk)                    1.813 MB
      Max allocated memory, integer                      248.968 kB
      Max allocated memory, logical                      138.320 kB
      Max allocated memory, character                      2.496 kB
      Max allocated memory, AOBATCH                      159.232 kB
      Max allocated memory, ODBATCH                        6.512 kB
      Max allocated memory, LSAOTENSOR                     7.296 kB
      Max allocated memory, SLSAOTENSOR                    6.624 kB
      Max allocated memory, ATOMTYPEITEM                 344.376 kB
      Max allocated memory, ATOMITEM                       3.696 kB
      Max allocated memory, LSMATRIX                      19.712 kB
      Max allocated memory, OverlapT                     113.600 kB
      Max allocated memory, linkshell                      1.260 kB
      Max allocated memory, integrand                     64.512 kB
      Max allocated memory, integralitem                 921.600 kB
      Max allocated memory, IntWork                       77.864 kB
      Max allocated memory, Overlap                        1.172 MB
      Max allocated memory, ODitem                         4.736 kB
      Max allocated memory, LStensor                     114.874 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   7.10 seconds
    >>> wall Time used in LSDALTON is   7.15 seconds

    End simulation
