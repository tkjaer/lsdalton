#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > acene_2_pariK.info <<'%EOF%'
   acene_2_pariK
   -------------
   Molecule:         C60/6-31G
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         9 min 30 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > acene_2_pariK.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=cc-pVTZdenfit
Dipeptide
EXCITATION DIAGNOSTIC
Atomtypes=2 Nosymmetry 
Charge=6.0 Atoms=14
C        0.0000000000            2.3061648982            1.3620932713
C        0.0000000000           -2.3061648982            1.3620932713
C        0.0000000000            2.3061648982           -1.3620932713
C        0.0000000000           -2.3061648982           -1.3620932713
C        0.0000000000            0.0000000000            2.6456927965
C        0.0000000000            0.0000000000           -2.6456927965
C        0.0000000000            4.6752972315            2.6516050030
C        0.0000000000           -4.6752972315            2.6516050030
C        0.0000000000            4.6752972315           -2.6516050030
C        0.0000000000           -4.6752972315           -2.6516050030
C        0.0000000000            6.8989898653            1.3443134000
C        0.0000000000           -6.8989898653            1.3443134000
C        0.0000000000            6.8989898653           -1.3443134000
C        0.0000000000           -6.8989898653           -1.3443134000
Charge=1.0 Atoms=10
H        0.0000000000            0.0000000000            4.6965033759
H        0.0000000000            0.0000000000           -4.6965033759
H        0.0000000000            4.6747079256            4.7008229428
H        0.0000000000           -4.6747079256            4.7008229428
H        0.0000000000            4.6747079256           -4.7008229428
H        0.0000000000           -4.6747079256           -4.7008229428
H        0.0000000000            8.6829888782            2.3490167870
H        0.0000000000           -8.6829888782            2.3490167870
H        0.0000000000            8.6829888782           -2.3490167870
H        0.0000000000           -8.6829888782           -2.3490167870

%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > acene_2_pariK.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**INTEGRALS
.PARI-K
**WAVE FUNCTIONS
.HF
*DENSOPT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >acene_2_pariK.check
cat >> acene_2_pariK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-XXX\.XXXXXX" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
